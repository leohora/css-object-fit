# Propriedade CSS: object-fit

A propriedade CSS é usada para especificar como um `<img>` ou ` <video>` deve ser redimensionado para caber em seu contêiner.

- **CONTAIN:** A imagem mantém sua proporção, mas é redimensionada para caber na dimensão especificada.

- **FILL:** Este é o padrão. A imagem é redimensionada para preencher a dimensão fornecida. Se necessário, a imagem será esticada ou comprimida para caber.

- **COVER:** A imagem mantém sua proporção e preenche a dimensão determinada. A imagem será cortada para caber.

- **SCALE-DOWN:** A imagem é reduzida para a versão menor.

- **NONE:** A imagem não é redimensionada.

## Redes Sociais
- [ ] [Instagram](https://www.instagram.com/leohoradev/)
- [ ] [YouTube](https://www.youtube.com/@LeoHoraDev)

## Contato
- [ ] leohoradev@gmail.com